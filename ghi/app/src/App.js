import logo from './logo.svg';
import './App.css';
import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList.js';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendeeForm from './AttendeeForm';

function App({attendees}) {
  if (attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container">
      <LocationForm />
      <ConferenceForm />
      <AttendeeForm />
    <AttendeesList attendees={attendees} />
    </div>
    </>
  );
}

export default App;
